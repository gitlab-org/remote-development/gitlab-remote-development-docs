# Project has been moved to https://gitlab.com/gitlab-org/workspaces/gitlab-workspaces-docs

See [last_version_before_deletion_and_archive](https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/tree/last_version_before_deletion_and_archive/)